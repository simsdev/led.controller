import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'controlLED',
      component: require('@/components/ControlLED').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
