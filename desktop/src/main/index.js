import { app, BrowserWindow, Menu, Tray, nativeImage } from 'electron'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

const iconName = 'icons/lamp-256x256.ico'
let iconPath = require('path').join(__dirname, iconName);
let mainWindow, tray
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createTray () {
  console.log(iconPath)
  tray = new Tray(nativeImage.createFromPath(iconPath))
  const contextMenu = Menu.buildFromTemplate([
    { label: 'Open', click: function() { mainWindow.show(); } },
    { label: 'Quit', click: function() { application.isQuiting = true; application.quit(); } }
])
  tray.setToolTip('LED Controller')
  tray.setContextMenu(contextMenu)

  tray.on('double-click', function(event) {
    mainWindow.show();
  })
}

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    width: 1000,
    useContentSize: true,
    autoHideMenuBar: true,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true
    },
    icon: nativeImage.createFromPath(iconPath)
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.on('minimize',function(event){
    event.preventDefault();
    mainWindow.hide();
  });

  // mainWindow.on('close', function (event) {
  //   if(!application.isQuiting) {
  //     event.preventDefault();
  //     mainWindow.minimize();
  //     return false;
  //   }
  // });

  // mainWindow.on('close', function (event) {
  //   if(!application.isQuiting){
  //       event.preventDefault();
  //       mainWindow.hide();
  //       event.returnValue = false;
  //   }
  // })

  mainWindow.on('show', function () {
    tray.setHighlightMode('always')
  })

  createTray()
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
