import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import axios from 'axios'

import VueCookies from 'vue-cookies'
import VueSession from 'vue-session'

Vue.use(VueCookies)
Vue.use(VueSession, { persist: true })

// set default config
VueCookies.config('1d')

Vue.config.productionTip = false
Vue.prototype.$http = axios

window.vueApp = new Vue({
  data: {
    apiURL: "http://localhost:3000",
    loading: false
  },
  computed: {},
  methods: {},
  created() {},
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
