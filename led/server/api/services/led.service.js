import l from '../../common/logger';
import db from './examples.db.service';
import { Control, Discovery } from 'magic-home';

class LedService {
    scan(timeout) {
        return new Discovery().scan(timeout).then(success => {
            return {
                status: true,
                result: success
            };
        }, error => {
            return {
                status: false,
                result: "Failed to retrieve the available devices",
                error: error
            };
        })
    }

    changeStatus(ip, status) {
        let statusText = (status ? "ON" : "OFF");
        let light = new Control(ip);
        return light.setPower(status).then(success => {
            return {
                status: true,
                result: "Lights " + statusText
            };
        }, error => {
            return {
                status: false,
                result: "Failed to turn the lights " + statusText,
                error: error
            };
        });
    }

    changeColor(ip, colors) {
        let light = new Control(ip);
        return light.setColorWithBrightness(colors[0], colors[1], colors[2], colors[3]).then(success => {
            return {
                status: true,
                result: "Color set!"
            };
        }, error => {
            return {
                status: false,
                result: "Failed to set color " + statusText,
                error: error
            };
        })
    }

    getStatus(ip) {
        let light = new Control(ip);
        return light.queryState().then(success => {
            return {
                status: true,
                result: success
            };
        }, error => {
            return {
                status: false,
                result: "Failed to retrieve status",
                error: error
            };
        });
    }
}

export default new LedService();
