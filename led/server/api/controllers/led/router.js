import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  .get('/scan', controller.scan)
  .get('/status/:ip', controller.status)
  .get('/turnOn/:ip', controller.turnOn)
  .get('/turnOff/:ip', controller.turnOff)
  .get('/changeColor/:ip/:color', controller.changeColor);
