import LedService from '../../services/led.service';

export class Controller {
  scan(req, res) {
    LedService.scan(2000).then(r => res.json(r));
  }

  turnOn(req, res) {
    LedService.changeStatus(req.params.ip, true).then(r => res.json(r));
  }

  turnOff(req, res) {
    LedService.changeStatus(req.params.ip, false).then(r => res.json(r));
  }

  changeColor(req, res) {
    let colors = req.params.color.split(',');
    
    if(colors.length < 4) {
      res.status(400).json({
        result: false,
        error: "Color is in invalid format. Must be 'R,G,B,W' separated by comma"
      }).end();
    } else {
      LedService.changeColor(req.params.ip, colors).then(r => res.json(r));
    }
  }

  status(req, res) {
    LedService.getStatus(req.params.ip).then(r => res.json(r));
  }
}
export default new Controller();
