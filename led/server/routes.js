import examplesRouter from './api/controllers/examples/router';
import ledRouter from './api/controllers/led/router';

export default function routes(app) {
  app.use('/api/v1/led', ledRouter);
  //app.use('/api/v1/examples', examplesRouter);
}
